import heapq
from heapq import heappop, heappush


def is_leaf(root):
    return root.left is None and root.right is None


# Узел дерева
class Node:
    def __init__(self, ch, freq, left=None, right=None):
        self.ch = ch
        self.freq = freq
        self.left = left
        self.right = right

    def __lt__(self, other):
        return self.freq < other.freq


# Пройти по дереву Хаффмана и сохранить коды Хаффмана в словаре
def encode(root, s, huffman_code):
    if root is None:
        return

    # обнаружил листовой узел
    if is_leaf(root):
        huffman_code[root.ch] = s if len(s) > 0 else '1'

    encode(root.left, s + '0', huffman_code)
    encode(root.right, s + '1', huffman_code)

def check_corect(text, words):
    for i in text:
        if i not in words:
            return True

# строит дерево Хаффмана
def build_huffman_tree(text):
    # Базовый случай: пустая строка
    if len(text) == 0:
        return

    # подсчитывает частоту появления каждого символа
    # и сохраните его в словаре

    with open("HUFF", 'r') as file:
        arr_char = file.readline().replace("\n", "").split(" ")
        arr_freq = file.readline().replace("\n", "").split(" ")
    print(arr_freq, arr_char)
    if check_corect(text, arr_freq):
        print("Error: Not correct input")
        return False
    freq = {i[0]: i[1] for i in zip(arr_char, arr_freq)}
    print(freq)

    # Создайте приоритетную очередь для хранения активных узлов дерева Хаффмана.
    pq = [Node(k, v) for k, v in freq.items()]
    heapq.heapify(pq)

    # делать до тех пор, пока в queue не окажется более одного узла
    while len(pq) != 1:
        # Удалить два узла с наивысшим приоритетом
        # (самая низкая частота) из queue

        left = heappop(pq)
        right = heappop(pq)

        # создает новый внутренний узел с этими двумя узлами в качестве дочерних и
        # с частотой, равной сумме частот двух узлов.
        # Добавьте новый узел в приоритетную очередь.

        total = left.freq + right.freq
        heappush(pq, Node(None, total, left, right))

    # `root` хранит указатель на корень дерева Хаффмана.
    root = pq[0]

    # проходит по дереву Хаффмана и сохраняет коды Хаффмана в словаре.
    huffman_code = {}
    encode(root, '', huffman_code)

    s = ''
    for c in text:
        s += huffman_code.get(c)

    print('The encoded string is:', s)

    for i in freq:
        print(i, huffman_code.get(i))


if __name__ == '__main__':
    text = input()
    build_huffman_tree(text)
