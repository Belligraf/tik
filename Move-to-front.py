def move2front_encode(strng, symboltable):
    sequence, pad = [], symboltable[::]
    for char in strng:
        indx = pad.index(char)
        sequence.append(indx)
        pad = [pad.pop(indx)] + pad
    return sequence


def move2front_decode(sequence, symboltable):
    chars, pad = [], symboltable.copy()
    for indx in sequence:
        char = pad[indx]
        chars.append(char)
        pad = [pad.pop(indx)] + pad
    return ''.join(chars)


def from_console():
    index = input("index: ").split()  # 0 1 4 2 3 1 4 1 4 4 2
    index = [int(i) for i in index]
    alphabet = input("alphabet: ").split()  # a b c d r
    decode = move2front_decode(index, alphabet)
    print('which decodes back to %r' % decode)


def from_file():
    file_name = input("file name: ")
    with open(file_name, 'r') as file:
        text = file.readlines()
        index = text[0].split()
        alphabet = text[1].split()

        index = [int(i) for i in index]
        decode = move2front_decode(index, alphabet)
        print('which decodes back to %r' % decode)


if __name__ == '__main__':
    # move2front_decode([0, 1, 4, 2, 3, 1, 4, 1, 4, 4, 2], ['a', 'b', 'c',2 'd', 'r'])
    while True:
        pick = input("1 read from file \n2 read from console\n")
        if pick == '1':
            from_file()
            continue
        elif pick == '2':
            from_console()
        else:
            print('bye')
            break
