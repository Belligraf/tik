def BWT_decode_fast(last_column, primary):
    first_column = ''.join(sorted(last_column))
    start = {}

    # заполняем start где key = символу, value = номер под которым он встречается
    for i in range(len(first_column)):
        if not first_column[i] in start:
            start[first_column[i]] = i
    links = []

    # заполняем links вписываем id символа из last_column
    for c in last_column:
        links.append(start[c])
        start[c] += 1
    ret = first_column[primary]
    i = links[primary]

    for _ in range(len(first_column) - 1):
        ret += first_column[i]
        i = links[i]
    print(ret)
    return ret[0] + ret[1:][::-1]


def from_console():
    word = input("word: ")
    index = int(input("index: "))  # a b c d r
    decode = BWT_decode_fast(word, index)
    print('which decodes back to %r' % decode)


def from_file():
    file_name = input("file name: ")
    with open(file_name, 'r') as file:
        text = file.readlines()[0].split()
        word = text[0]
        index = int(text[1])
        decode = BWT_decode_fast(word, index)
        print('which decodes back to %r' % decode)


if __name__ == '__main__':
    while True:
        pick = input("1 read from file \n2 read from console\n")
        if pick == '1':
            from_file()
            continue
        elif pick == '2':
            from_console()
        else:
            print('bye')
            break

# print(BWT_decode_fast('bbbeeeaaabbb', 4))
# print(BWT_decode_fast('annb$aa', 4))
